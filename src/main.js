var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var arraySize = 9
var arrowArray = create2DArray(arraySize, arraySize, 0, true)
var arrowSize = 0.80
var defaultSize = 0.40
var walker = {
  backward: [[Math.floor(arraySize * 0.5) + 1, Math.floor(arraySize * 0.5) + 1], [Math.floor(arraySize * 0.5) + 1, Math.floor(arraySize * 0.5) + 2], [Math.floor(arraySize * 0.5) + 1, Math.floor(arraySize * 0.5) + 3]],
  foreward: [[Math.floor(arraySize * 0.5) - 1, Math.floor(arraySize * 0.5) - 1], [Math.floor(arraySize * 0.5) - 1, Math.floor(arraySize * 0.5) - 2], [Math.floor(arraySize * 0.5) - 1, Math.floor(arraySize * 0.5) - 3]]
}
var directions = create2DArray(arraySize, arraySize, 0, true)
for (var i = 0; i < walker.backward.length; i++) {
  arrowArray[walker.backward[i][0]][walker.backward[i][1]] = (arrowSize - defaultSize)
  directions[walker.backward[i][0]][walker.backward[i][1]] = 0.5
}
for (var i = 0; i < walker.foreward.length; i++) {
  arrowArray[walker.foreward[i][0]][walker.foreward[i][1]] = (arrowSize - defaultSize)
  directions[walker.foreward[i][0]][walker.foreward[i][1]] = -0.5
}
var neighbours
var direction

var frames = 0
var viewState = false

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < arrowArray.length; i++) {
    for (var j = 0; j < arrowArray[i].length; j++) {
      if (viewState === false) {
        drawArrow(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5)) * (42 / 768) * boardSize * (17 / arraySize), windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5)) * (42 / 768) * boardSize * (17 / arraySize), (44 / 768) * boardSize * (17 / arraySize) * (arrowArray[i][j] + defaultSize), directions[i][j], defaultSize + arrowArray[i][j])
      } else {
        drawArrow(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5)) * (42 / 768) * boardSize * (17 / arraySize), windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5)) * (42 / 768) * boardSize * (17 / arraySize), (44 / 768) * boardSize * (17 / arraySize) * (arrowArray[i][j] + defaultSize), directions[i][j], defaultSize + arrowArray[i][j])
      }
    }
  }

  if (viewState === false) {
    frames += deltaTime * 0.025
    if (frames > 1) {
      frames = 0
      neighbours = getNeighbours(walker.backward[walker.backward.length - 1])
      if (neighbours.length !== 0) {
        var randomNeighbour = Math.floor(Math.random() * neighbours.length)
        walker.backward.push(neighbours[randomNeighbour])
        if (walker.backward.length > 3) {
          walker.backward.splice(0, 1)
          direction = getDirection(walker.backward)
          if (direction !== undefined) {
            directions[walker.backward[1][0]][walker.backward[1][1]] = direction[0]
            directions[walker.backward[2][0]][walker.backward[2][1]] = direction[1]
          }
        }
        arrowArray[walker.backward[walker.backward.length - 1][0]][walker.backward[walker.backward.length - 1][1]] = (arrowSize - defaultSize)
      }
      neighbours = getNeighbours(walker.foreward[walker.foreward.length - 1])
      if (neighbours.length !== 0) {
        var randomNeighbour = Math.floor(Math.random() * neighbours.length)
        walker.foreward.push(neighbours[randomNeighbour])
        if (walker.foreward.length > 3) {
          walker.foreward.splice(0, 1)
          direction = getDirection(walker.foreward)
          if (direction !== undefined) {
            directions[walker.foreward[1][0]][walker.foreward[1][1]] = direction[0]
            directions[walker.foreward[2][0]][walker.foreward[2][1]] = direction[1]
          }
        }
        arrowArray[walker.foreward[walker.foreward.length - 1][0]][walker.foreward[walker.foreward.length - 1][1]] = (arrowSize - defaultSize)
      }
      for (var i = 0; i < arrowArray.length; i++) {
        for (var j = 0; j < arrowArray[i].length; j++) {
          if (arrowArray[i][j] > 0) {
            arrowArray[i][j] -= 0.005 + 0.045 * (mouseX / windowWidth)
          } else {
            arrowArray[i][j] = 0
          }
        }
      }
    }
  }
}

function mousePressed() {
  if (viewState === true) {
    viewState = false
  } else {
    viewState = true
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function getDirection(walker, type) {
  var posX1 = walker[0][0]
  var posX2 = walker[1][0]
  var posX3 = walker[2][0]
  var posY1 = walker[0][1]
  var posY2 = walker[1][1]
  var posY3 = walker[2][1]
  var directions
  // RR
  if (posX1 === posX2 - 1 && posX2 === posX3 - 1) {
    directions = [0, 0]
  }
  // LL
  if (posX1 === posX2 + 1 && posX2 === posX3 + 1) {
    directions = [1, 1]
  }
  // DD
  if (posY1 === posY2 - 1 && posY2 === posY3 - 1) {
    directions = [0.5, 0.5]
  }
  // UU
  if (posY1 === posY2 + 1 && posY2 === posY3 + 1) {
    directions = [-0.5, -0.5]
  }
  // RU
  if (posX1 === posX2 - 1 && posY2 === posY3 + 1) {
    directions = [-0.25, -0.5]
  }
  // DL
  if (posY1 === posY2 - 1 && posX2 === posX3 + 1) {
    directions = [0.75, 1]
  }
  // DR
  if (posY1 === posY2 - 1 && posX2 === posX3 - 1) {
    directions = [0.25, 0]
  }
  // LU
  if (posX1 === posX2 + 1 && posY2 === posY3 + 1) {
    directions = [-0.75, -0.5]
  }
  // RD
  if (posX1 === posX2 - 1 && posY2 === posY3 - 1) {
    directions = [0.25, 0.5]
  }
  // UL
  if (posY1 === posY2 + 1 && posX2 === posX3 + 1) {
    directions = [-0.75, 1]
  }
  // UR
  if (posY1 === posY2 + 1 && posX2 === posX3 - 1) {
    directions = [-0.25, 0]
  }
  // LD
  if (posX1 === posX2 + 1 && posY2 === posY3 - 1) {
    directions = [0.75, 0.5]
  }
  return directions
}

function getNeighbours(walker) {
  var neighbours = []
  var posX = walker[0]
  var posY = walker[1]
  if (posX > 0) {
    if (arrowArray[posX - 1][posY] === 0) {
      neighbours.push([posX - 1, posY])
    }
  }
  if (posX < arraySize - 1) {
    if (arrowArray[posX + 1][posY] === 0) {
      neighbours.push([posX + 1, posY])
    }
  }
  if (posY > 0) {
    if (arrowArray[posX][posY - 1] === 0) {
      neighbours.push([posX, posY - 1])
    }
  }
  if (posY < arraySize - 1) {
    if (arrowArray[posX][posY + 1] === 0) {
      neighbours.push([posX, posY + 1])
    }
  }
  return neighbours
}

function drawArrow(posX, posY, size, rotation, color) {
  fill(color * 255)
  noStroke()
  push()
  translate(posX, posY)
  push()
  rotate(rotation * Math.PI)
  rect(0, 0, size, size * 0.125)

  push()
  translate(size * 0.4, -size * 0.125 * sqrt(2) + size * 0.125 * sqrt(2) * 0.25)
  push()
  rotate(0.25 * Math.PI)
  rect(0, 0, size * 0.5, size * 0.125)
  pop()
  pop()

  push()
  translate(size * 0.4, size * 0.125 * sqrt(2) - size * 0.125 * sqrt(2) * 0.25)
  push()
  rotate(-0.25 * Math.PI)
  rect(0, 0, size * 0.5, size * 0.125)
  pop()
  pop()

  pop()
  pop()
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
